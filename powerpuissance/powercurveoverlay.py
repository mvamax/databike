from IPython.core.display import display, HTML
import random
from string import Template

def ppoverlay(time, data,data2,cp):
    if len(time) != len(data):
        return None
    r = str(random.randint(0,10000))
    template = Template("""
        <html>
        <head>
        <title>titre</title>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="http://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/annotations.js"></script>
        </head>
        <body>

        <div id="container$r" style="width: 1000px; height: 600px; margin: 125 auto"></div>

        <script language="JavaScript">
               function secondsToHms(d) {
                    d = Number(d);
                    var h = Math.floor(d / 3600);
                    var m = Math.floor(d % 3600 / 60);
                    var s = Math.floor(d % 3600 % 60);
                    if (h<9){h= "0"+h;}
                    if (m<9){m= "0"+m;}
                    if (s<9){s= "0"+s;}
                    if(h=="00"){
                        if(m=="00"){
                            return (s) +'s'; 
                        }else{
                             return (m+"m "+ s + "s"); 
                        }
                    }
                    else{
                        return (h+"h "+m+"m "+ s+'s');
                    }
                }
               var data = $d;
                var data2 = $d2;
                     var data3 = $d3;
                var options2 = {
                      title: {
                        text: null
                      },
                        tooltip: {
                        shared: true,
                        crosshairs: true,
                   formatter: function () {
                            return  '<b>'+secondsToHms(this.x)+'</b><br\> watts:'+this.y+' <br\> model:'+this.points[1].y;
                        }
                    },
                      subtitle: {
                        text: null
                      },
                      xAxis: {
                        type: 'logarithmic',
                        tickInterval: 1,
                        tickPositions: [1, 2, 5, 10, 30, 60, 120, 300, 600,1200,3600,7200].map((v) => Math.log10(v))
                      },
                      yAxis: {
                        title: {
                          text: 'Power'
                        },
                        plotLines: [{
                          value: 0,
                          width: 1,
                          color: '#808080'
                        }]
                      },
                      plotOptions: {
                            areaspline: {
                                fillOpacity: 0.3
                            }
                        },
                      series: [{
                        type: 'spline',
                        name: 'power',
                        data: data
                      },{
                        type: 'areaspline',
                        name: 'model',
                        data: data2,
                        color : "#CBC6C4"
                      },{
                        type: 'areaspline',
                        name: 'model',
                        data: data3,
                        color: '#999695'
                      }
                      ],
                  annotations: [{
                                      labelOptions: {
                            shape: 'circle',
                             
                        },
                        labels: [{
                            point: { x: 5, y: 500,xAxis:0,yAxis:0 },
                            text: 'Anaerobic Work Capacity'
                        }]
                    }]
                    }

           Highcharts.chart('container$r', options2)

        </script>

        </body>
        </html>
    """)
    
    d = [[i,j] for i, j in zip(time, data)]
        
    d2 = [[i,j] for i, j in zip(time, data2)]
    
    d3 = [[i,cp] for i in time]
    
    f = template.substitute(r=str(r),d=str(d),d2=str(d2),d3=str(d3))
    display(HTML(f))
