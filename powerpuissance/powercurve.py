from IPython.core.display import display, HTML
import random
from string import Template

def powercurve(time, data):
    if len(time) != len(data):
        return None
    r = str(random.randint(0,10000))
    template = Template("""
        <html>
        <head>
        <title>titre</title>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        </head>
        <body>

        <div id="container$r" style="width: 1000px; height: 600px; margin: 125 auto"></div>

        <script language="JavaScript">
               function secondsToHms(d) {
                    d = Number(d);
                    var h = Math.floor(d / 3600);
                    var m = Math.floor(d % 3600 / 60);
                    var s = Math.floor(d % 3600 % 60);
                    if (h<9){h= "0"+h;}
                    if (m<9){m= "0"+m;}
                    if (s<9){s= "0"+s;}
                    if(h=="00"){
                        if(m=="00"){
                            return (s) +' s'; 
                        }else{
                             return (m+"m"+ s + "s"); 
                        }
                    }
                    else{
                        return (h+"h"+m+"m"+ s+'s');
                    }
                }
               var data = $d;
               
                var options2 = {
                      title: {
                        text: null
                      },
                        tooltip: {
                        formatter: function () {
                            return  secondsToHms(this.x)+':'+this.y+' watts';
                        }
                    },
                      subtitle: {
                        text: null
                      },
                      xAxis: {
                        type: 'logarithmic',
                        tickInterval: 1,
                        tickPositions: [1, 2, 5, 10, 30, 60, 120, 300, 600,1200,3600,7200].map((v) => Math.log10(v))
                      },
                      yAxis: {
                        title: {
                          text: 'Power'
                        },
                        plotLines: [{
                          value: 0,
                          width: 1,
                          color: '#808080'
                        }]
                      },
                      series: [{
                        type: 'spline',
                        name: 'power',
                        data: data
                      }]
                    }

           Highcharts.chart('container$r', options2)

        </script>

        </body>
        </html>
    """)
    
    d = [[i,j] for i, j in zip(time, data)]
    d = str(d)
    f = template.substitute(r=str(r),d=str(d))
    display(HTML(f))